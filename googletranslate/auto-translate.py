#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# vim: ai ts=4 sts=4 et sw=4

# Este programa traduci automaticamente directorios de archivos .po

"""Automatic translation using m$ translator
    :copyright: 2012 Andrew Colin Kissa
    :copyright: 2011 by Openlabs Technologies & Consulting (P) Limited
    :license: BSD, see LICENSE for more details.
"""

import os
import re
import time

from optparse import OptionParser

from polib import pofile
from googletranslate import translate


def format_date():
    "Return a date string in required format"
    return time.strftime("%Y-%m-%d %R+0200", time.strptime(time.ctime()))


def first_pass(items, thestring):
    "replace %(xxx)s vars"
    for item in items:
        thestring = thestring.replace(item, '|^^|', 1)
    return thestring


def second_pass(items, thestring):
    "replace %s with actual %(xxx)s"
    for item in items:
        thestring = thestring\
        .replace('|^^|', item, 1)\
        .replace('| ^ ^ |', item, 1)
    return thestring


def getpofs(matched, dirname, files):
    "utility to get po files"
    matched.extend([os.path.join(dirname, filename)
                    for filename in files
                    if filename.endswith('.po')])


def get_lang(dirname):
    "Get the language from directory name"
    return os.path.basename(
        os.path.dirname(
            os.path.dirname(dirname)
        )
    )


def process(raw_entry, source_lang, language, regex):
    if source_lang == language:
        return raw_entry
    "Process and Translate the string"
    languages_bidi = ["he", "ar", "fa", "yi"]
    found = regex.findall(raw_entry)
    if found:
        if language in languages_bidi:
            return None
        raw_entry = first_pass(found, raw_entry)
    new_entry = translate(raw_entry, source_lang, language)
    if found:
        new_entry = second_pass(found, new_entry)
    return new_entry


def createps(filename, source_lang):
    "update po file"
    do_save = False
    print "Processing: %s" % filename
    pobj = pofile(filename)
    lang = get_lang(filename)

    match_re = re.compile(r'((?:%\([^\W]{1,}\)(?:s|d))|(?:{{\w+}}))')

    for entry in pobj.untranslated_entries():
        msgstr = process(entry.msgid, source_lang, lang, match_re)
        if entry.msgid_plural:
            if msgstr:
                entry.msgstr_plural['0'] = msgstr
            msgstr_plural = process(entry.msgid_plural, source_lang, lang, match_re)
            if msgstr_plural:
                entry.msgstr_plural['1'] = msgstr_plural
        else:
            if msgstr:
                entry.msgstr = msgstr
        do_save = True

    if do_save:
        pobj.metadata['PO-Revision-Date'] = format_date()
        pobj.save(filename)


if __name__ == '__main__':
    # Run tings mon

    usage = "usage: %prog directory"
    parser = OptionParser(usage)
    parser.add_option('-s', '--source', dest="source_lang", default="en")
    opts, arguments = parser.parse_args()

    if len(arguments) != 1:
        parser.error("Please specify the directory to process")

    directory = arguments[0]

    if not os.path.exists(directory):
        parser.error("Directory: %s does not exist" % directory)

    try:
        pofiles = []
        os.path.walk(directory, getpofs, pofiles)
        _ = [createps(path, opts.source_lang)
             for path in pofiles]
    except KeyboardInterrupt:
        print "\nCTRL-C pressed, exiting"