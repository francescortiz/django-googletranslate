#-*- coding: UTF-8 -*-
import types
from django.utils.translation import ugettext_lazy as _, get_language
from googletranslate.fields import AutoTranslateField

__author__ = 'franki'

AUTO_TRANSLATE_GROUPS = {}


def is_autotranslated(self):
    current_language = get_language()
    for field in self._meta.fields:
        if hasattr(field, 'autotranslate_group') and hasattr(field, 'language'):
            if field.language == current_language and getattr(self, field.name):
                return True
    return False


def contribute_to_class(self, cls, name, *args, **kwargs):
    auto_translate_field = None
    for field in cls._meta.fields:
        if field.name == name:
            auto_translate_field = field
            break
    for cls2 in cls.__bases__:
        try:
            for field in cls2._meta.fields:
                if field.name == name:
                    auto_translate_field = field
                    break
        except AttributeError:
            pass

    if not auto_translate_field:
        auto_translate_field = AutoTranslateField(language=self.autotranslate.language, default=False, verbose_name=_(
            'leave this checked unless you are not using the microsoft translation. Read microsoft translations terms in case of doubt.'))
        auto_translate_field.contribute_to_class(cls, '%s_at' % name)

    auto_translate_field.autotranslate = self.autotranslate

    try:
        AUTO_TRANSLATE_GROUPS[str(cls)]
    except KeyError:
        AUTO_TRANSLATE_GROUPS[str(cls)] = {}

    try:
        AUTO_TRANSLATE_GROUPS[str(cls)][self.autotranslate.group_name]
    except KeyError:
        AUTO_TRANSLATE_GROUPS[str(cls)][self.autotranslate.group_name] = []

    AUTO_TRANSLATE_GROUPS[str(cls)][self.autotranslate.group_name].append(self)

    auto_translate_field.autotranslate_group = AUTO_TRANSLATE_GROUPS[str(cls)][self.autotranslate.group_name]
    cls.autotranslate_groups = AUTO_TRANSLATE_GROUPS[str(cls)]
    cls.is_autotranslated = is_autotranslated

    return self.contribute_to_class__autotranslate(cls, name, *args, **kwargs)


class auto_translate(object):
    def __init__(self, language, *group_name):
        self.language = language
        self.group_name = group_name

    def __call__(self, field):
        self.field = field
        field.autotranslate = self
        field.contribute_to_class__autotranslate = field.contribute_to_class
        field.contribute_to_class = types.MethodType(contribute_to_class, field)
        return field
