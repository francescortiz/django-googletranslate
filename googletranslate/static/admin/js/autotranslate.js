/**
 * User: franki
 * Date: 3/27/13
 * Time: 11:54 PM
 */

if (!window.autotranslate_enabled) {
    window.autotranslate_enabled = true;

    if (!$) {
        $ = django.jQuery;
    }


    $(function () {
        var locales = ['it','es','en'];
        var locales_str = ',it,es,en,';

        if (!$('.autotranslate-source').length) {
            var inputs = $('input[type="text"], textarea');
            for (var i = 0; i < inputs.length; i++) {
                var input = inputs[i];
                var jinput = $(input);
                var locale = input.name.split('_').pop();
                if (locales_str.indexOf(','.concat(locale).concat(',')) != -1) {
                    var field_name = input.name.split('_');
                    field_name.pop();
                    field_name = field_name.join('_');
                    console.log(field_name);
                    var html = "Translate from: "
                    for (var j = 0; j < locales.length; j++) {
                        var l = locales[j];
                        if (locale != l) {
                            html = html
                                .concat('<a class="autotranslate-source" href="" ')
                                .concat(' from-field="').concat(field_name).concat('_').concat(l).concat('"')
                                .concat(' from-language="').concat(l).concat('"')
                                .concat(' to-field="').concat(field_name).concat('_').concat(locale).concat('"')
                                .concat(' to-language="').concat(locale).concat('"')
                                .concat('>')
                                .concat(l)
                                .concat('</a> ');
                        }
                    }
                    $('<div>'.concat(html).concat('</div>')).insertAfter(jinput);
                }
            }
        }

        $('.autotranslate-source').closest('.form-row').css({
            'border-bottom': '3px solid black'
        });
        $('.autotranslate-source').click(function (e) {
            var jthis = $(this);
            var from_field = this.getAttribute('from-field');
            var from_language = this.getAttribute('from-language');
            var to_field = this.getAttribute('to-field');
            var to_language = this.getAttribute('to-language');
            // Lo aplicamos 2 veces, una por si es inline y la otra por si es stacked
            jthis.closest('tr.form-row').find('.field-' + to_field + '_at').find('input').attr('checked', 'checked');
            jthis.closest('div.form-row').parent().find('.field-' + to_field + '_at').find('input').attr('checked', 'checked');

            if (typeof CKEDITOR !== "undefined") {
                for (instance in CKEDITOR.instances) {
                    var ck = CKEDITOR.instances[instance]
                    $(ck.element.$).val(ck.getData());
                }
            }

            var text = jthis.closest('fieldset').find('.field-' + from_field).find('textarea, input').val();
            $.ajax({
                url: '/arruncuchuncu/',
                data: {
                    'text': text,
                    'to_language': to_language,
                    'from_language': from_language
                },
                dataType: 'json',
                error: function () {
                    alert('Error de traducción, vuelve a intentar.');
                },
                success: function (data) {
                    // Lo aplicamos 2 veces, una por si es inline y la otra por si es stacked
                    var target = jthis.closest('tr.form-row').find('.field-' + to_field).find('textarea, input');
                    target.val(data.translation);
                    var target = jthis.closest('div.form-row').parent().find('.field-' + to_field).find('textarea, input');
                    target.val(data.translation);
                    if (typeof CKEDITOR !== "undefined") {
                        for (instance in CKEDITOR.instances) {
                            var ck = CKEDITOR.instances[instance]
                            ck.setData($(ck.element.$).val());
                        }
                    }
                }
            });
            e.preventDefault();
            e.stopPropagation();
            return false;
        });

    });

}