# -*- coding: UTF-8 -*-

from django import forms
from django.conf import settings
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.forms.util import flatatt
from django.utils.encoding import force_text
from django.utils.text import capfirst
from django.utils.translation import ugettext as _

class AutoTranslateFormWidget(forms.CheckboxInput):

    def __init__(self, field, attrs=None):
        self.field = field
        super(AutoTranslateFormWidget, self).__init__(attrs)

    # def _format_value(self, value):
    #     return unicode(value)

    def render(self, name, value, attrs=None):

        final_attrs = self.build_attrs(attrs, type='checkbox', name=name)
        if self.check_test(value):
            final_attrs['checked'] = 'checked'
        if not (value is True or value is False or value is None or value == ''):
            # Only add the 'value' attribute if a value is non-empty.
            final_attrs['value'] = force_text(value)
        checkbox = format_html('<input{0} />', flatatt(final_attrs))
        sources = ' <script src="'+settings.STATIC_URL+'admin/js/autotranslate.js" async="true"></script>' + capfirst(_("translate from:"))
        for field in self.field.autotranslate_group:
            if field.autotranslate.language != self.field.language:
                sources += ' <a href="#" class="autotranslate-source"' \
                           ' from-field="%(from_field)s"' \
                           ' from-language="%(from_language)s"' \
                           ' to-field="%(to_field)s"' \
                           ' to-language="%(to_language)s"' \
                           '>%(from_language)s</a> ' % {
                    'from_field': field.name,
                    'from_language': field.autotranslate.language,
                    'to_field': self.field.autotranslate.field.name,
                    'to_language': self.field.language,
                }


        return mark_safe(checkbox + sources)


class AutoTranslateFormField(forms.Field):

    # widget = AutoTranslateFormWidget

    def __init__(self, field, **kwargs):
        # kwargs['required'] = False
        self.field = field
        self.widget = auto_translate_form_widget_generator(field)()
        super(AutoTranslateFormField, self).__init__(kwargs)

    # def clean(self, value):
    #     value = self.to_python(value)
    #     return value


class auto_translate_form_field_generator(object):

    def __init__(self, field):
        self.field = field

    def __call__(self, *args, **kwargs):
        form_field = AutoTranslateFormField(self.field, *args, **kwargs)
        return form_field


class auto_translate_form_widget_generator(AutoTranslateFormWidget):

    def __init__(self, field):
        self.field = field

    def __call__(self, *args, **kwargs):
        widget = AutoTranslateFormWidget(self.field, *args, **kwargs)
        return widget