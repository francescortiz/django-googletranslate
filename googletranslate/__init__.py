#-*- coding: UTF-8 -*-
from django.conf import settings

from apiclient.discovery import build


GOOGLE_TRANSLATE_KEY = getattr(settings, 'GOOGLE_TRANSLATE_KEY', '')


def translate(text, from_language, to_language):
    service = build('translate', 'v2',
                    developerKey=GOOGLE_TRANSLATE_KEY)

    response = service.translations().list(
        source=from_language,
        target=to_language,
        q=text
    ).execute()

    translatedText = response['translations'][0]['translatedText']

    return translatedText


def translations(text, from_language, to_language):
    service = build('translate', 'v2',
                    developerKey=GOOGLE_TRANSLATE_KEY)

    response = service.translations().list(
        source=from_language,
        target=to_language,
        q=text
    ).execute()

    translatedTexts = []
    for trans in response['translations']:
        translatedTexts.append(trans['translatedText'])

    return translatedTexts