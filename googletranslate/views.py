#-*- coding: UTF-8 -*-
import json
from django.http.response import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from googletranslate import translate


__author__ = 'franki'


def translate_google(request):
    try:
        text = request.GET['text']
    except MultiValueDictKeyError:
        return HttpResponse("")

    try:
        to_language = request.GET['to_language']
    except MultiValueDictKeyError:
        return HttpResponse(text)

    from_language = request.GET.get('from_language', None)

    translation = translate(text, from_language, to_language)

    data = {
        'translation': translation
    }

    response = HttpResponse(json.dumps(data))
    response['Content-Type'] = "application/json"
    return response
