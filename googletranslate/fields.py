# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from googletranslate.forms import auto_translate_form_field_generator

class AutoTranslateField(models.BooleanField):

    description = "A field that tells us if the related field has been auto translated."

    def __init__(self, language=None, *args, **kwargs):
        self.language = language
        super(AutoTranslateField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'form_class': auto_translate_form_field_generator(self)}
        defaults.update(kwargs)
        return super(AutoTranslateField, self).formfield(**defaults)


try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^googletranslate\.fields\.AutoTranslateField$"])
except ImportError:
    pass
